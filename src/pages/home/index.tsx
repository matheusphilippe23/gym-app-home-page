import * as React from "react";
import type { HeadFC, PageProps } from "gatsby";
import AppHeader from "@/components/header";
import RoundCard, { IRoundCard } from "@/components/round-card";

import Pt1 from "@/images/pt1.png";
import Pt2 from "@/images/pt2.png";
import Pt3 from "@/images/pt3.png";
import SectionHeader from "../../components/section-header";

const trainersMock: IRoundCard[] = [
  {
    image: Pt1,
    name: "João",
  },
  {
    image: Pt2,
    name: "Pedro",
  },
  {
    image: Pt3,
    name: "Flavia",
  },
];

const IndexPage: React.FC<PageProps> = () => {
  return (
    <main>
      <AppHeader />
      <SectionHeader title="Personal trainers" />
      <div className="overflow-x-scroll ">
        <div className="w-fit flex flex-row justify-start items-center gap-[8px]  p-[24px] pb-[8px]">
          {trainersMock.map((trainer) => (
            <RoundCard {...trainer} />
          ))}
        </div>
      </div>
    </main>
  );
};

export default IndexPage;

export const Head: HeadFC = () => <title>Home Page</title>;
