import { motion } from "framer-motion";
import * as React from "react";

interface ISectionHeader {
  title: string;
}

const SectionHeader = ({ title }: ISectionHeader) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.5 }}
      className="w-full flex flex-row justify-start items-center gap-[8px] p-[24px] pb-[8px] pt-[32px]"
    >
      <span className="text-[24px] font-[400]">{title}</span>
    </motion.div>
  );
};

export default SectionHeader;
