import * as React from "react";
import { motion } from "framer-motion";
export interface IRoundCard {
  image: string;
  name: string;
}
const RoudCard = ({ image, name }: IRoundCard) => {
  return (
    <motion.div
      initial={{ opacity: 0, scale: 0.5 }}
      animate={{ opacity: 1, scale: 1 }}
      transition={{ duration: 0.5 }}
      className="w-[150px] h-[150px] rounded-full relative "
    >
      <img
        src={image}
        className="w-[150px] h-[150px] object-cover  rounded-full"
      ></img>
      <div className="absolute bottom-[12px] right-[4px] bg-[color:var(--secondary)] border-l-[2px] border-b-[2px] border-[#fff] rounded-full p-[4px] px-[18px]">
        <span className="text-[color:var(--text-color-secondary)]  font-[800] text-[18px] uppercase">
          {name}
        </span>
      </div>
    </motion.div>
  );
};

export default RoudCard;
