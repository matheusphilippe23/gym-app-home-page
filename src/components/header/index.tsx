import * as React from "react";
import logo from "@/images/logo.png";
import { motion } from "framer-motion";
const AppHeader = () => {
  return (
    <motion.div
      initial={{ opacity: 0, y: -80 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}
      className="w-full flex flex-row justify-center items-center gap-[8px] border-b-[1px] border-[color:var(--gray100)] p-[24px] pb-[8px]"
    >
      <img src={logo} className="max-w-[60px] max-h-[60px]"></img>
      <span>02 Fitness</span>
    </motion.div>
  );
};

export default AppHeader;
